# dns-scan

Query DNS server for hostnames and IPs

```bash
Usage of ./😸:
  -a string
        Name lookup from IP; example: 10.0.0.2
  -d string
        DNS server ipaddress; example: 10.0.0.1
  -i string
        IP lookup from Name; example: duckduckgo.com
  -r string
        Scan Network ID range; example: 10.0.0.
  -t int
        Connection timeout in seconds (default 10)

```


## output

```shell script
$ go run . -d 10.0.0.1 -r 10.0.0.

10.0.0.1 -> OpenWrt.lan.
10.0.0.2 -> re9000.lan.
10.0.0.3 -> eap225.lan.
10.0.0.11 -> pi-server.lan.
10.0.0.12 -> pi-services.lan.
10.0.0.21 -> dv808.lan.
10.0.0.22 -> ring-driveway.lan.
10.0.0.23 -> cc-pro.lan.
10.0.0.24 -> cc-test.lan.
10.0.0.25 -> cc-master.lan.
10.0.0.26 -> pi-5x5.lan.
10.0.0.27 -> pi-4x4.lan.
10.0.0.28 -> pi3.lan.
10.0.0.30 -> tv-master.lan.
10.0.0.35 -> ps4.lan.
10.0.0.39 -> nest.lan.
10.0.0.41 -> s8.lan.
10.0.0.42 -> n10.lan.
10.0.0.61 -> scott-arch.lan.
10.0.0.62 -> hp-laptop.lan.
10.0.0.63 -> iyler-pc.lan.
10.0.0.200 -> espressif.lan.


```